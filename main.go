package main

import (
	"fmt"
	"log"

	r "gopkg.in/rethinkdb/rethinkdb-go.v6"

	"github.com/gocolly/colly"
)

//Quotes es el tipo de dato que queremos guardar
type Quotes struct {
	Quote string
	Title string
}

func main() {
	//DataBase with rethinkdb
	session, err := r.Connect(r.ConnectOpts{
		Addresses: []string{"localhost:28015", "localhost:28016"},
		Database:  "test",
		AuthKey:   "eae300a2-bde1-47db-9c22-f2bfb4e45d42",
	})
	//Error if we can not connect to database
	if err != nil {
		log.Fatalln("Could not connect")
	}

	//Insert table to database
	err = r.DB("test").TableCreate("quotes").Exec(session)
	if err != nil {
		log.Fatal("Could not create table")
	}

	//Scraping with colly
	citas := make([]Quotes, 0)
	//Instantiate default collector colly framework
	c := colly.NewCollector(
		colly.AllowedDomains("quotes.toscrape.com", "www.quotes.toscrape.com"),
	)

	//Before making a request print "Visiting ..."
	c.OnRequest(func(r *colly.Request) {
		r.Headers.Set("content-type", "application/json; charset=utf-8")
		fmt.Println("Visiting", r.URL)
	})

	// Find quotes with colly
	c.OnHTML("div[class=quote]", func(e *colly.HTMLElement) {

		cita := Quotes{
			Quote: e.ChildText("span.text"),
			Title: e.ChildText("small.author"),
		}

		citas = append(citas, cita)
	})

	// Find and visit next page links
	c.OnHTML(`.next a[href]`, func(e *colly.HTMLElement) {
		link := e.Attr("href")

		//In case do not find a link
		if link == "" {
			log.Println("No link found", e.Request.URL)
		}
		// Print link
		fmt.Printf("Link found: %s\n", link)
		e.Request.Visit(link)
	})

	c.OnResponse(func(r *colly.Response) {
		fmt.Println("Visited", r.Request.URL)
	})

	c.Visit("http://quotes.toscrape.com/")

	_, err = r.Table("quotes").Insert(citas).RunWrite(session)
	if err != nil {
		log.Fatal(err)
	}

}
